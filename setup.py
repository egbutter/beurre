#!/usr/bin/env python

from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(
    name='beurre',
    packages=['beurre'],
    version='0.1',
    description='utility modules for python',
    long_description=readme(),
    author='e',
    author_email='e@kr4d.xyz',
    license='MIT',
    zip_safe=False,
    install_requires=[
        'python-dateutil>=2.2',
        'chardet>=2.3.0',
        'mandrill>=1.0.57',
        ],
    setup_requires=['pytest-runner'],
    tests_require=[
        'pytest',
        'WebTest',
        'web.py',
        'flask',
        ]
    )
