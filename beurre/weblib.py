# encoding: utf-8

import sys
import os
import re
import urllib
import urlparse
import posixpath
import base64
import json
import datetime
import time
import io
import cProfile
import pstats
from uuid import uuid4
from functools import wraps, update_wrapper

from dateutil import parser

try:
    import web  # only required for a few obvious decorator functions
except ImportError:
    web = None

try:
    import flask
except ImportError:
    flask = None

import util

import logging
logger = logging.getLogger(__name__)


# convenient placeholders for cookielib.Cookie
# allows us to quickly make Cookie(name='', value='', **COOKIE_DEFAULTS)
COOKIE_DEFAULTS = dict(
    version=0,
    domain='',
    domain_specified=False,
    domain_initial_dot=False,
    port=None,
    port_specified=False,
    path='/',
    path_specified=True,
    secure=False,
    expires=None,
    discard=True,
    comment=None,
    comment_url=None,
    rest={'HttpOnly': None},
    rfc2109=False,
    )


#
# django-like model/view mashups
#

def get_or_create(session, model, **kw):
    """a la django
    """
    obj = session.query(model).filter_by(**kw).first()
    if not obj:
        obj = model(**kw)
        session.add(obj)
        session.flush()
    return obj


def paged(order_by_df, per_page_df):
    """decorator to pass in default order / page / per page for pagination

    #. acquire the thread-local request object
    #. calculate pagination order by / offset / limit from request object
    #. patch the info into a database connection

    WARNING: careful not to patch MULTIPLE queries within the controller
    """
    def wrapper(query_fn):
        @wraps(query_fn)
        def paged_fn(*args, **kwargs):
            req = web.input()
            cn = web.ctx.cntc
            logger.warn("patching with req: {}".format(id(req)))
            logger.warn("patching over cn: {}".format(id(cn)))
            if req.has_key('f_'):
                logger.warn("Using filter f_={}, NOT PAGED"\
                                .format(req.get('f_', '')))
                return query_fn(*args, **kwargs)
            order_by = req.get('o_', order_by_df)
            order_by_dir = req.get('d_', 'a')=='d' and " DESC " or ""
            page = int(req.get('p_', 0))
            per_page = int(req.get('n_', per_page_df))
            offset = page*per_page
            limit = per_page
            web.ctx.cntc.paged = (order_by+order_by_dir, offset, limit,)
            ds = query_fn(*args, **kwargs)
            ds.page = page
            ds.per_page = per_page
            ds.total = web.ctx.cntc.paged_total
            return ds
        return paged_fn
    return wrapper


#
# commonly reused decorators
#

def cors_webpy(app, **kw):
    """wrap a web.py controller with headers for cross origin resource sharing
    especially useful for views using resources from many websites
    see https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
    for more information on CORS

    >>> app = web.application(('/mashup', 'mashup',), globals())

    >>> origins = ['mixpanel.com', 'mapbox.com']
    >>> methods = ['GET', 'HEAD', 'POST', 'PUT']
    >>> @cors_webpy(app, origin=origins, methods=methods, max_age=22)
    ... class mashup:
    ...     def GET(self):
    ...         return 'This page makes js calls to mixpanel and mapbox'

    >>> b = app.browser()
    >>> headers = b.open("/mashup").headers
    >>> 'mixpanel.com' in headers.get('Access-Control-Allow-Origin')
    True
    >>> 'mapbox.com' in headers.get('Access-Control-Allow-Origin')
    True
    >>> 'true' == headers.get('Access-Control-Allow-Credentials')
    True
    >>> 'POST' in headers.get('Access-Control-Allow-Methods')
    True
    >>> 'HEAD' in headers.get('Access-Control-Allow-Methods')
    True
    >>> '22' == headers.get('Access-Control-Max-Age')
    True
    """
    origin = kw.get('origin')
    credentials = kw.get('credentials', True)
    methods = kw.get('methods')
    headers = kw.get('headers')
    max_age = kw.get('max_age', 21600)
    attach_to_all = kw.get('attach_to_all', True)
    automatic_options = kw.get('automatic_options', True)

    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, datetime.timedelta):
        max_age = max_age.total_seconds()

    def allowed_methods(f):
        return [m for m in ['GET', 'HEAD', 'POST', 'PUT', 'DELETE']
                        if hasattr(f, m)]

    def get_methods(f):
        if methods is not None:
            return methods
        return allowed_methods(f)

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and web.ctx.method == 'OPTIONS':
                methods = allowed_methods(f)
                web.header('Allow', methods)
                return f(*args, **kwargs)
            if not attach_to_all and web.ctx.method != 'OPTIONS':
                return f(*args, **kwargs)

            h = web.header
            h('Access-Control-Allow-Origin', origin)
            h('Access-Control-Allow-Methods', get_methods(f))
            h('Access-Control-Allow-Credentials', str(credentials).lower())
            h('Access-Control-Max-Age', str(max_age))
            if headers is not None:
                h('Access-Control-Allow-Headers', headers)
            return f(*args, **kwargs)

        return update_wrapper(wrapped_function, f)
    return decorator


def cors_flask(app, **kw):
    """wrap a flask controller with headers to allow cross origin resource sharing
    especially useful for views using resources from many websites

    >>> app = flask.Flask(__name__)

    >>> origins = ['mixpanel.com', 'mapbox.com']
    >>> methods = ['GET', 'HEAD', 'POST', 'PUT']
    >>> @app.route('/mashup')
    ... @cors_flask(app, origin=origins, methods=methods, max_age=22)
    ... def mashup():
    ...     return 'This page makes js calls to mixpanel and mapbox'

    we inject the allowed CORS domains ahead of time
    >>> client = app.test_client()
    >>> headers = client.get('/mashup').headers
    >>> 'mixpanel.com' in headers.get('Access-Control-Allow-Origin')
    True
    >>> 'mapbox.com' in headers.get('Access-Control-Allow-Origin')
    True
    >>> 'true' == headers.get('Access-Control-Allow-Credentials')
    True
    >>> 'POST' in headers.get('Access-Control-Allow-Methods')
    True
    >>> 'HEAD' in headers.get('Access-Control-Allow-Methods')
    True
    >>> '22' == headers.get('Access-Control-Max-Age')
    True
    """
    origin = kw.get('origin')
    credentials = kw.get('credentials', True)
    methods = kw.get('methods')
    headers = kw.get('headers')
    max_age = kw.get('max_age', 21600)
    attach_to_all = kw.get('attach_to_all', True)
    automatic_options = kw.get('automatic_options', True)

    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, datetime.timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and flask.request.method == 'OPTIONS':
                resp = app.make_default_options_response()
            else:
                resp = flask.make_response(f(*args, **kwargs))
            if not attach_to_all and flask.request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Allow-Credentials'] = str(credentials).lower()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator


def authd(checker_fn, fallback_fn):
    """simple decorator that checks if a user meets an auth criterion

    === a web.py example

    >>> from cookielib import Cookie
    >>> import web

    >>> urls = ('/another', 'another', '/(.*)', 'echo',)
    >>> app = web.application(urls, globals())

    >>> def forbid():
    ...     raise web.forbidden()

    >>> @authd(lambda: web.cookies().get('allowed'), forbid)
    ... class echo:
    ...     def GET(self, name):
    ...         return name

    >>> class another:
    ...     def GET(self):
    ...         return 'get'
    ...     @authd(lambda: web.cookies().get('allowed'), forbid)
    ...     def POST(self):
    ...         return 'post'

    >>> b = app.browser()
    >>> allowed = Cookie(name='allowed', value='1', **COOKIE_DEFAULTS)

    without the cookie, we cannot get to any controllers
    >>> b.open("/test").read()
    'forbidden'

    once we add the cookie, we can
    >>> b.cookiejar.set_cookie(allowed)
    >>> b.open("/test").read()
    'test'
    >>> b.reset()

    we can also restrict only certain methods
    >>> b.open('/another').read()
    'get'
    >>> b.open('/another', data='x=1').read()
    'forbidden'
    >>> b.cookiejar.set_cookie(allowed)
    >>> b.open('/another', data='x=1').read()
    'post'


    === a flask example

    >>> app = flask.Flask(__name__)

    >>> @app.route('/restricted')
    ... @authd(lambda: flask.request.cookies.get('allowed'), lambda: flask.abort(403))
    ... def restricted_page():
    ...     return 'seekret!!!'

    can only access restricted controller when cookie 'allowed' is set
    >>> client = app.test_client()
    >>> '403 Forbidden' in client.get('/restricted').data
    True
    >>> client.set_cookie('', 'allowed', '1')
    >>> client.get('/restricted').data
    'seekret!!!'
    """
    def decorator(f):
        def authd_fn(*args, **kwargs):
            if not checker_fn():
                return fallback_fn()
            return f(*args, **kwargs)
        return update_wrapper(authd_fn, f)
    return decorator


#
# cross site request forgery
#

def xsrf_token():
    """x-site request forgery protection token
    TODO add the xsrf tokens to forms
    """
    if not web.ctx.session.has_key('xsrf'):
        web.ctx.session.xsrf = uuid4().hex  # better use sha?
    return web.ctx.session.xsrf


def xsrf_protected(fn):
    """decorator protecting PUT/POST requests from session riding
    TODO decorate controllers for xsrf prot forms
    """
    def dec_fn(*args,**kwargs):
        req = web.input()
        if not (req.has_key('xsrf') and req.xsrf==web.ctx.session.pop('xsrf',None)):
            raise web.badrequest()
        return fn(*args,**kwargs)
    return dec_fn


#
# simple web.py internationalization
#

def i18n_loadhook():
    """hook i18n into lang middleware

    >>> app.add_processor(web.loadhook(i18n_loadhook))  # doctest: +SKIP
    """
    web.ctx.lang = web.input(lang=None, _method="GET").lang

def custom_gettext(string):
    """translate a given string to the language of the application
    via http://webpy.org/cookbook/i18n_support_in_template_file
    """
    translation = web.ctx.lang and load_translations(web.ctx.lang)
    if translation is None:
        return unicode(string)
    return translation.ugettext(string)


#
# decorators on REST api
#

VALID_KEY = re.compile('[a-zA-Z0-9_-]{1,255}')

def valid_api_key(key):
    """check if key is suitable hash, if matches a validated user
    """
    if VALID_KEY.match(key) is not None:
        return User.get_active_key(key)
    return False

def requires_api_key(fn):
    """controller decorator defining how to allow api access
    (protects against directory traversal attacks / rolling perm's)
    """
    def decorated_fn(*args, **kwargs):
        if not kwargs.has_key('key') or not valid_api_key(kwargs['key']):
            web.badrequest()
        return fn(*args, **kwargs)
    return decorated_fn


#
# HACK to transform web.py subapps into breadcrumbs
#

def build_breadcrumb(ctx):
    "introspect web.py app_stack to build a reasonable breadcrumb"
    paths = map(lambda x: x.fvars.get('breadcrumb',''), web.ctx.app_stack)
    names = [' '.join(_.title() for _ in path.strip('/').split('_')) for path in paths]
    paths[0], names[0] = ctx.realhome, 'Home'
    paths = util.accumulate(paths)
    pathsnames = zip(paths, names)
    links = ['<a href="%s/">%s</a>' % _ for _ in pathsnames]
    to_render = " >> ".join(links)
    return to_render


def breadcrumbify(url_app_tuple):
    """assuming web.py style mapping, patch url mapping into subapps
    """
    for i, app_or_url in enumerate(url_app_tuple):
        if isinstance(app_or_url, web.application):
            app_or_url.fvars['breadcrumb'] = url_app_tuple[i-1]
    return url_app_tuple


#
# other rando website utility methods
#

def make_url(path, **params):
    """consistent url generation
    - pass in arbitrary params inspired by `werkzeug.urls.Href`
    - always assume traditional multiple params (do not overwrite)
    - to overwrite, use special method `__replace__`
    - to ignore certain params, use `__ignore__` (handy for ajax back and forth)

    >>> ignore_fn = lambda x: x.startswith('_')
    >>> kw = dict(fuz=1, biz="boo")
    >>> xx = make_url('/foo/', _format='excel', __ignore__=ignore_fn, **kw)
    >>> '_format' in xx
    False
    >>> 'fuz=1' in xx
    True
    >>> 'biz=boo' in xx
    True
    >>> yy = make_url('/foo/?bar=1', _format='excel', **kw)
    >>> 'bar=1' in yy
    True
    >>> zz = make_url('/foo/', **{'bar':1, 'fuz':(1,2,), 'biz':"boo"})
    >>> 'fuz=1' in zz
    True
    >>> 'fuz=2' in zz
    True
    >>> qq = make_url('/foo/?a=1&a=2')
    >>> 'a=1' in qq
    True
    >>> 'a=2' in qq
    True
    """
    replace = params.pop('__replace__', {})
    ignore = params.pop('__ignore__', None)

    parsed = list(urlparse.urlparse(path))
    query = urlparse.parse_qsl(parsed[4])

    for k,v in query:
        if k in params:
            this = params[k]
            if hasattr(this, '__iter__'):
                this.append(v)
            else:
                this = [this]+[v]
            params[k] = this
        else:
            params[k] = v

    params.update(replace)

    if ignore:
        params = {k: v for k,v in params.items() if not ignore(k)}

    parsed[4] = urllib.urlencode(params, doseq=True)
    cleanpath = urlparse.urlunparse(parsed)
    return cleanpath


def prefix_urls(pathpfx, classpfx, urls):
    newurls = []
    for i in range(0, len(urls), 2):
        newurls.append(pathpfx + urls[i])
        newurls.append(classpfx + urls[i + 1])
    return tuple(newurls)


def safe_join(directory, filename):
    """Safely join `directory` and `filename`.  If this cannot be done,
    this function returns ``None``.
    via github.com/mitsuhiko/werkzeug security.py
    """
    _os_alt_seps = list(sep for sep in [os.path.sep, os.path.altsep]
                    if sep not in (None, '/'))
    filename = posixpath.normpath(filename)
    for sep in _os_alt_seps:
        if sep in filename:
            return None
    if os.path.isabs(filename) or filename.startswith('../'):
        return None
    return os.path.join(directory, filename)


def local_or_static_join(static, somepath):
    "infer if user is referring to template in working dir or in static"
    localpath = os.path.abspath(somepath)
    staticpath = safe_join(static, somepath)
    if os.path.exists(localpath):
        return localpath
    elif os.path.exists(staticpath):
        return staticpath
    raise IOError("That template does not exist on your path or in local package.")


def inject_file(x):
    "little wrapper for injecting css, js, etc, for html email templates"
    with open(x, "rb") as f:
        return f.read()


def inject_image(x):
    "base64 encoded code to put in src of an image tag in html"
    _, ext = os.path.splitext(x)
    with open(x, "rb") as f:
        code = base64.b64encode(f.read())
        return "data:image/{};base64,{}".format(ext.strip('.'), code)


def scale(color, pct):
    def clamp(l, x, h):
        return min(max(l, x), h)

    if len(color) == 4:
        r, g, b = color[1], color[2], color[3]
        r = r + r
        g = g + g
        b = b + b
    else:
        r, g, b = color[1:3], color[3:5], color[5:]
    r = int(r, 16)
    g = int(g, 16)
    b = int(b, 16)
    r = clamp(0, int(r * pct + 0.5), 255)
    g = clamp(0, int(g * pct + 0.5), 255)
    b = clamp(0, int(b * pct + 0.5), 255)
    return '#%X%X%X' % (r, g, b)


def render_field(field):
    "render either web.py or Django form "
    def get_error(field):
        if hasattr(field, 'note'):
            return field.note
        if hasattr(field, 'errors'):
            return ', '.join(field.errors)
        return None

    def to_html(field):
        if isinstance(field, web.form.Input):
            return field.render()
        else:
            return unicode(field)

    html = []
    error = get_error(field)
    if error:
        html.append('<span class="flderr" title="%s">' % error)
    html.append(to_html(field))
    if error:
        html.append('</span>')
    return '\n'.join(html)


class JSONEncoderISODate(json.JSONEncoder):
    """json encoder adding assumption of ISO Date format

    >>> JSONEncoderISODate().encode({'dt': datetime.date(2014, 10, 2)})
    '{"dt": "2014-10-02"}'
    """
    def default(self, obj):
        if isinstance(obj, (datetime.date, datetime.datetime)):
            return obj.isoformat()
        return super(JSONEncoderISODate, self).default(obj)


class JSONDecoderISODate(json.JSONDecoder):
    """json decoder parsing arbitrary date formats

    >>> JSONDecoderISODate().decode('{"dt": "2014-10-02"}')
    {u'dt': datetime.datetime(2014, 10, 2, 0, 0)}
    """
    def __init__(self, **kw):
        super(JSONDecoderISODate, self).__init__(
                    object_hook=self._parse_date_hook, **kw)

    def _parse_date_hook(self, obj):
        if isinstance(obj, dict):
            for key in obj:
                if isinstance(obj[key], basestring):
                    try:
                        obj[key] = parser.parse(obj[key])
                    except (ValueError,TypeError):
                        pass
        return obj


class ProfileMiddleware(object):
    """generic wsgi middleware for profiling wsgi calls
    WARNING: should always be last middleware loaded:
      #. you want profile everything else
      #. for speed, we return the result NOT the wrapped func

    >>> import logging
    >>> from cStringIO import StringIO
    >>> mock = StringIO()
    >>> sh = logging.StreamHandler(mock)
    >>> log = logging.getLogger('ProfilerTest')
    >>> log.setLevel(logging.DEBUG)
    >>> log.addHandler(sh)

    >>> import web
    >>> from webtest import TestApp

    >>> web.config.debug = False
    >>> urls = ('/', 'index',)
    >>> app = web.application(urls, globals())
    >>> profiled_app = ProfileMiddleware(app.wsgifunc(), log=log)

    >>> class index:
    ...     def GET(self):
    ...         return "Hello, world."

    >>> t = TestApp(profiled_app)
    >>> 'Hello' in t.get("/")
    True
    >>> sh.flush()
    >>> would_log = mock.getvalue()
    >>> print would_log  # doctest: +ELLIPSIS
    Run finished in ... seconds
        ...
    <BLANKLINE>
    >>> 'Ordered by: internal time' in would_log
    True
    >>> 'due to restriction <20>' in would_log
    True
    >>> mock.close()

    >>> mock_cum = StringIO()
    >>> log.handlers[0].stream = mock_cum  # patch a clean buffer
    >>> by_cum = ProfileMiddleware(app.wsgifunc(), log=log, sort='name', count=30)
    >>> t_cum = TestApp(by_cum)
    >>> 'Hello' in t_cum.get("/")
    True
    >>> sh.flush()
    >>> would_log_cum = mock_cum.getvalue()
    >>> 'Ordered by: function name' in would_log_cum
    True
    >>> 'due to restriction <30>' in would_log_cum
    True
    >>> mock_cum.close()
    """

    def __init__(self, func, log=None, sort='time', count=20):
        self.func = func
        self.log = log
        self.sort = sort
        self.count = count

    def __call__(self, env, resp):
        stime = time.time()
        pr = cProfile.Profile()
        pr.enable()
        result = pr.runcall(self.func, env, resp)
        pr.disable()
        etime = time.time() - stime
        self.log.info("Run finished in {} seconds".format(etime))

        with io.BytesIO() as s:
            ps = pstats.Stats(pr, stream=s).sort_stats(self.sort)
            ps.print_stats(self.count)
            self.log.debug(s.getvalue())

        return result


def logerror(olderror, logger):
    """wrap internalerror function to log the traceback too

    >>> import web
    >>> web.config.debug = False
    >>> urls = ('/', 'index',)

    >>> import logging
    >>> from cStringIO import StringIO
    >>> logger = logging.getLogger('LogTest')
    >>> stderr = sys.stderr
    >>> sys.stderr = mock = StringIO()
    >>> sh = logging.StreamHandler()
    >>> sh.setLevel(logging.ERROR)
    >>> logger.addHandler(sh)

    >>> app = web.application(urls, globals())
    >>> app.internalerror = logerror(app.internalerror, logger)

    >>> class index:
    ...     def GET(self):
    ...         return None+1

    >>> b = app.browser()
    >>> 'internal server error' in b.open("/").read()
    True
    >>> sys.stderr = stderr
    >>> 'TypeError' in mock.getvalue()
    True
    """
    def logerror_fn():
        theerr = olderror()
        _,exc,_ = sys.exc_info()
        logger.exception(exc)
        return theerr
    return logerror_fn


if __name__ == "__main__":
    import doctest
    doctest.testmod()
