"""beaut backport of lru cache from python 3.3 to 2.7
http://code.activestate.com/recipes/578078-py26-and-py30-backport-of-python-33s-lru-cache/

Full-featured O(1) LRU cache backported from Python3.3.

The full Py3.3 API is supported:
 - thread safety,
 - maxsize,
 - keyword args,
 - type checking,
 - __wrapped__,
 - and cache_info.

Includes Py3.3 optimizations for better memory utilization,
fewer dependencies, and fewer dict lookups.

Documentation and examples:
http://docs.python.org/dev/library/functools.html#functools.lru_cache

The Python3.3 update added the typed option for
differentiating equal arguments differing types.
This makes it possible to cache different results for f(1) and for f(1.0).

Python3.3 also added a `__wrapped__` attribute that can be used for bypassing
or replacing the cache.

The Python 3.3 version was also refactored, beautified, and optimized.
It consumes less memory than the 3.2 version. It makes half as many hash calls.
And, it inlines the relevant parts of `OrderedDict`
while eliminating unused functionality. It relies exclusively on Python basics
such as 1) plain python lists to implement a circular doubly linked list,
2) a plain python dictionary, and 3) a plain tuple used
for storing the positional arguments, sorted keyword arguments,
and optional type information.

The backport works on Python2.6 or later, Python3.0 or later, Python with Psyco,
and with PyPy.

The backport entailed replacing Python 3's nonlocal keyword with nested scope
lookup of a mutable list (for the hit/miss statistics). Also,the `__wrapped__`
attribute was added manually (because older versions `functools.wraps` didn't
support it).

The original 3.3 is at:
http://hg.python.org/cpython/file/97b0cf9df420/Lib/functools.py#l139

To backport to even earlier versions of Python, replace the named tuple with an
equivalent handmade class and replace the with-statement with an equivalent try/finally.

No print-statements were harmed during this backport ;-)


>>> import urllib2
>>> from contextlib import closing

>>> @lru_cache(maxsize=32)
... def get_pep(num):
...     "Retrieve text of a Python Enhancement Proposal"
...     pep_url = 'http://www.python.org/dev/peps/pep-%04d/' % num
...     try:
...         with closing(urllib2.urlopen(pep_url)) as req:
...             return req.read()
...     except urllib2.HTTPError:
...         pass

>>> found = 0
>>> for n in 8, 320, 8, 320, 279, 320, 9991:
...     pep = get_pep(n)
...     if pep: found += 1
>>> found
6

>>> get_pep.cache_info()
CacheInfo(hits=3, misses=4, maxsize=32, currsize=4)

>>> @lru_cache(maxsize=None)
... def fib(n):
...     if n < 2:
...         return n
...     return fib(n-1) + fib(n-2)

>>> [fib(n) for n in range(16)]
[0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610]

>>> fib.cache_info()
CacheInfo(hits=28, misses=16, maxsize=None, currsize=16)
"""

from collections import namedtuple
from functools import update_wrapper
from threading import RLock


_CacheInfo = namedtuple("CacheInfo", ["hits", "misses", "maxsize", "currsize"])

class _HashedSeq(list):
    __slots__ = 'hashvalue'

    def __init__(self, tup, hash=hash):
        self[:] = tup
        self.hashvalue = hash(tup)

    def __hash__(self):
        return self.hashvalue


def _make_key(args, kwds, typed,
             kwd_mark = (object(),),
             fasttypes = {int, str, frozenset, type(None)},
             sorted=sorted, tuple=tuple, type=type, len=len):
    'Make a cache key from optionally typed positional and keyword arguments'
    key = args
    if kwds:
        sorted_items = sorted(kwds.items())
        key += kwd_mark
        for item in sorted_items:
            key += item
    if typed:
        key += tuple(type(v) for v in args)
        if kwds:
            key += tuple(type(v) for k, v in sorted_items)
    elif len(key) == 1 and type(key[0]) in fasttypes:
        return key[0]
    return _HashedSeq(key)


def lru_cache(maxsize=100, typed=False):
    """Least-recently-used cache decorator.

    If *maxsize* is set to None, the LRU features are disabled and the cache
    can grow without bound.

    If *typed* is True, arguments of different types will be cached separately.
    For example, f(3.0) and f(3) will be treated as distinct calls with
    distinct results.

    Arguments to the cached function must be hashable.

    View the cache statistics named tuple (hits, misses, maxsize, currsize) with
    f.cache_info().  Clear the cache and statistics with f.cache_clear().
    Access the underlying function with f.__wrapped__.

    See:  http://en.wikipedia.org/wiki/Cache_algorithms#Least_Recently_Used
    """

    # Users should only access the lru_cache through its public API:
    #       cache_info, cache_clear, and f.__wrapped__
    # The internals of the lru_cache are encapsulated for thread safety and
    # to allow the implementation to change (including a possible C version).

    def decorating_function(user_function):

        cache = dict()
        stats = [0, 0]                  # make statistics updateable non-locally
        HITS, MISSES = 0, 1             # names for the stats fields
        make_key = _make_key
        cache_get = cache.get           # bound method to lookup key or return None
        _len = len                      # localize the global len() function
        lock = RLock()                  # because linkedlist updates aren't threadsafe
        root = []                       # root of the circular doubly linked list
        root[:] = [root, root, None, None]      # initialize by pointing to self
        nonlocal_root = [root]                  # make updateable non-locally
        PREV, NEXT, KEY, RESULT = 0, 1, 2, 3    # names for the link fields

        if maxsize == 0:

            def wrapper(*args, **kwds):
                # no caching, just do a statistics update after a successful call
                result = user_function(*args, **kwds)
                stats[MISSES] += 1
                return result

        elif maxsize is None:

            def wrapper(*args, **kwds):
                # simple caching without ordering or size limit
                key = make_key(args, kwds, typed)
                result = cache_get(key, root)   # root used here as a unique not-found sentinel
                if result is not root:
                    stats[HITS] += 1
                    return result
                result = user_function(*args, **kwds)
                cache[key] = result
                stats[MISSES] += 1
                return result

        else:

            def wrapper(*args, **kwds):
                # size limited caching that tracks accesses by recency
                key = make_key(args, kwds, typed) if kwds or typed else args
                with lock:
                    link = cache_get(key)
                    if link is not None:
                        # record recent use of the key by moving it to the front of the list
                        root, = nonlocal_root
                        link_prev, link_next, key, result = link
                        link_prev[NEXT] = link_next
                        link_next[PREV] = link_prev
                        last = root[PREV]
                        last[NEXT] = root[PREV] = link
                        link[PREV] = last
                        link[NEXT] = root
                        stats[HITS] += 1
                        return result
                result = user_function(*args, **kwds)
                with lock:
                    root, = nonlocal_root
                    if key in cache:
                        # getting here means that this same key was added to the
                        # cache while the lock was released.  since the link
                        # update is already done, we need only return the
                        # computed result and update the count of misses.
                        pass
                    elif _len(cache) >= maxsize:
                        # use the old root to store the new key and result
                        oldroot = root
                        oldroot[KEY] = key
                        oldroot[RESULT] = result
                        # empty the oldest link and make it the new root
                        root = nonlocal_root[0] = oldroot[NEXT]
                        oldkey = root[KEY]
                        oldvalue = root[RESULT]
                        root[KEY] = root[RESULT] = None
                        # now update the cache dictionary for the new links
                        del cache[oldkey]
                        cache[key] = oldroot
                    else:
                        # put result in a new link at the front of the list
                        last = root[PREV]
                        link = [last, root, key, result]
                        last[NEXT] = root[PREV] = cache[key] = link
                    stats[MISSES] += 1
                return result

        def cache_info():
            """Report cache statistics"""
            with lock:
                return _CacheInfo(stats[HITS], stats[MISSES], maxsize, len(cache))

        def cache_clear():
            """Clear the cache and cache statistics"""
            with lock:
                cache.clear()
                root = nonlocal_root[0]
                root[:] = [root, root, None, None]
                stats[:] = [0, 0]

        wrapper.__wrapped__ = user_function
        wrapper.cache_info = cache_info
        wrapper.cache_clear = cache_clear
        return update_wrapper(wrapper, user_function)

    return decorating_function
