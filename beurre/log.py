# encoding: utf-8

import os
import socket
import sys
import copy
import urllib2
import smtplib
import logging
from logging.handlers import SMTPHandler, HTTPHandler
from email import encoders
from email.utils import formatdate
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from datetime import datetime
from functools import wraps
from contextlib import closing

import mandrill

import console
from util import unicode_or_bust


class MachineFilter(logging.Filter):
    def filter(self, record):
        record.machine = socket.gethostname()
        return True


class WebServerFilter(logging.Filter):
    """== Create a logging.Filter with wsgi webserver context info

    >>> import web  # doctest: +SKIP
    >>> ip_fn = lambda: web.ctx.get('ip')  # doctest: +SKIP
    >>> user_fn = lambda: web.ctx.session.get('user')  # doctest: +SKIP
    >>> handler.addFilter(WebServerFilter(ip_fn, user_fn))  # doctest: +SKIP

    >>> import flask  # doctest: +SKIP
    >>> ip_fn = lambda: flask.request.remote_addr  # doctest: +SKIP
    >>> user_fn = lambda: flask.session.get('user')  # doctest: +SKIP
    >>> handler.addFilter(WebServerFilter(ip_fn, user_fn))  # doctest: +SKIP
    """
    def __init__(self, ip_fn=lambda: '', user_fn=lambda: ''):
        self.ip_fn = ip_fn
        self.user_fn = user_fn

    def filter(self, record):
        ipaddr = self.ip_fn() or ''
        if not ipaddr:
            record.ip = ipaddr
        else:
            try:
                hostname, aliases, _ = socket.gethostbyaddr(ipaddr)
            except socket.error:
                hostname = ipaddr
            record.ip = hostname
        record.user = self.user_fn() or ''
        return True


class PreambleFilter(logging.Filter):
    "write a preamble to a StreamHandler"
    def __init__(self, app='', args='', setup='',
                statuses=('succeeded', 'failed',), failno=40):
        self.cmd_app = app
        self.cmd_args = args
        self.cmd_setup = setup
        self._success, self._failure = statuses
        self.cmd_status = self._success
        self.failno = failno

    def filter(self, record):
        record.cmd_app = self.cmd_app
        record.cmd_args = self.cmd_args
        record.cmd_setup = self.cmd_setup
        if record.levelno >= self.failno:
            self.cmd_status = self._failure
        record.cmd_status = self.cmd_status
        return True


class StderrStreamLogger(object):
    """patch over stderr to log print statements to INFO
    placeholders isatty and fileno mimic python stream
    stderr still accessible at stderr.__stderr__
    """
    def __init__(self, logger):
        self.logger = logger
        self.level = logging.INFO
        self.linebuf = ''

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.log(self.level, line.rstrip())

    def isatty(self):
        return False

    def fileno(self):
        return None


def colorize(fn):
    """this decorator assumes logging handler with stream
    converts stream to colored output, cross-platform
    """
    def colorized_fn(*args):
        logger = args[0]
        record = copy.copy(args[1])
        other_args = args[2:] if len(args)>2 else []
        levelno = record.levelno
        if not logger.is_tty:  # no access to terminal
            return fn(logger, record, *other_args)
        if os.name=='nt':  # windows
            color = console.choose_color_windows(levelno)
        else:
            color = console.choose_color_ansi(levelno)
        with console.set_color(color, stream=logger.stream):
            return fn(logger, record, *other_args)
    return colorized_fn


class ColoredStreamHandler(logging.StreamHandler):
    """coloring stream handler in logging module
    """
    def __init__(self):
        super(ColoredStreamHandler, self).__init__()

    @property
    def is_tty(self):
        """no need to colorize output to other processes
        """
        return stream_is_tty(self.stream)

    @property
    def std_or_stderr(self):
        """we only want to do this if we get fileno eq to stdout/err
        """
        fileno = getattr(self.stream, 'fileno', None)
        return fileno and fileno() in (sys.stdout.fileno(), sys.stderr.fileno())

    @colorize
    def emit(self, record):
        """calls parent StreamHandler emit after colorizing
        """
        super(ColoredStreamHandler, self).emit(record)


class ColoredHandler(object):
    "mixin for any logging.Handler trying to generate text and html messages"

    def getSubject(self, record):
        subject = self.subject % record.__dict__
        return subject

    def _format_record(self, record):
        color = self._choose_color_html(record.levelno)
        text = unicode_or_bust(self.format(record))
        html = u'<pre style="color:{};">{}</pre>'.format(color, text)
        return text, html

    def _choose_color_html(self, levelno):
        if(levelno>=40):
            color = '#EE0000'
        elif(levelno>=30):
            color = '#DAA520'
        elif(levelno>=20):
            color = '#228B22'
        elif(levelno>=10):
            color = '#D0D2C4'
        else:
            color = '#000'
        return color


class ColoredSMTPHandler(ColoredHandler, SMTPHandler):
    "emits html-colored email, one per log message .. also formats subject"

    def __init__(self, *args, **kwargs):
        self.ssl = kwargs.pop('ssl', False)
        super(ColoredSMTPHandler, self).__init__(*args, **kwargs)

    def emit(self, record):
        try:
            msg = self._build_html_msg(record)
            text, html = self._format_record(record)
            html = '<html><head></head><body>{}</body></html>'.format(html)
            msg.attach(MIMEText(text, 'text'))
            msg.attach(MIMEText(html, 'html'))
            self._send_html_msg(msg.as_string())
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

    def _build_html_msg(self, record):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = self.getSubject(record)
        msg['From'] = self.fromaddr
        msg['To'] = ",".join(self.toaddrs)
        msg['Date'] = formatdate()
        return msg

    def _send_html_msg(self, msg):
        port = self.mailport
        if not port:
            port = smtplib.SMTP_PORT
        if self.ssl:
            smtp = smtplib.SMTP_SSL(self.mailhost, port)
        else:
            smtp = smtplib.SMTP(self.mailhost, port)
        if self.secure is not None:
            smtp.ehlo()
            smtp.starttls(*self.secure)
            smtp.ehlo()
        if self.username:
            smtp.login(self.username, self.password)
        smtp.sendmail(self.fromaddr, self.toaddrs, msg)
        smtp.quit()


class TwistedSMTPHandler(object):
    """twisted mixin to make a deferred version of our SMTPHandlers
    """

    def _send_html_msg(self, msg):
        from OpenSSL.SSL import SSLv3_METHOD
        from twisted.mail.smtp import ESMTPSenderFactory
        from twisted.internet.ssl import ClientContextFactory
        from twisted.internet.defer import Deferred
        from twisted.internet import reactor
        port = self.mailport
        if not port:
            port = smtplib.SMTP_PORT
        dfd = Deferred()
        kw = {}
        if self.ssl:
            kw['contextFactory'] = ClientContextFactory()
            kw['contextFactory'].method = SSLv3_METHOD
        senderFactory = ESMTPSenderFactory(
            self.username,
            self.password,
            self.fromaddr,
            self.toaddrs,
            msg,
            dfd,
            **kw)
        reactor.connectTCP(self.mailhost, port, senderFactory)
        return dfd


def patch_webdriver(this_logger, this_webdriver):
    patched = 0
    for h in this_logger.handlers:
        if isinstance(h, (ScreenshotColoredSMTPHandler, ScreenshotColoredMandrillHandler,)):
            h.webdriver = this_webdriver
            this_logger.warn("Patching handler {} {}"\
                        .format(repr(h), logging.getLevelName(h.level)))
            patched += 1


class ScreenshotColoredSMTPHandler(ColoredSMTPHandler):
    """== Email context around a failed email scrape ==
    - need to initialize with selenium webdriver or runtime `patch_webdriver`
    - take a screenshot of the current page where an exception was raised
    - also saved the url and page source, for debugging purposes
    """
    def __init__(self, *args, **kwargs):
        self.webdriver = kwargs.pop('webdriver', None)
        super(ScreenshotColoredSMTPHandler, self).__init__(*args, **kwargs)

    def _build_html_msg(self, record):
        msg = MIMEMultipart()
        msg['Subject'] = self.getSubject(record)
        msg['From'] = self.fromaddr
        msg['To'] = ",".join(self.toaddrs)
        msg['Date'] = formatdate()
        return msg

    def emit(self, record):
        if self.webdriver is None:  # lose the message
            return
        name = 'screenshot.png'
        src_name = 'page_source.txt'
        try:
            msg = self._build_html_msg(record)
            text, html = self._format_record(record)
            url = self.webdriver.current_url
            lk = '<div><a href="{}">{}</a></div>'.format(url, url)
            html = '<html><head></head><body>{}{}<img src="cid:{}"/></body></html>'\
                            .format(html, lk, name)
            msg.attach(MIMEText(text, 'text'))
            msg.attach(MIMEText(html, 'html'))
            img = MIMEBase('image', 'png')
            img.set_payload(self.webdriver.get_screenshot_as_base64())
            img.add_header('Content-ID', name)
            img.add_header('Content-Disposition', 'attachment', filename=name)
            msg.attach(img)
            src = MIMEBase('application', 'octet-stream')
            src.set_payload(self.webdriver.page_source)
            try:
                encoders.encode_base64(src)
            except UnicodeEncodeError:
                page_source_utf = self.webdriver.page_source.encode('utf-8')
                src.set_payload(page_source_utf)
                encoders.encode_base64(src)
            src.add_header('Content-ID', src_name)
            src.add_header('Content-Disposition', 'attachment', filename=src_name)
            msg.attach(src)
            self._send_html_msg(msg.as_string())
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)


class BufferedColoredSMTPHandler(ColoredSMTPHandler):
    "Get as much of a job log as possible, esp. useful for distributed jobs"

    def __init__(self, *args, **kwargs):
        capacity = kwargs.pop('capacity', 1024)
        flushLevel = kwargs.pop('flushLevel', logging.ERROR)
        super(BufferedColoredSMTPHandler, self).__init__(*args, **kwargs)
        self.capacity = capacity
        self.flushLevel = flushLevel
        self.buffer = []

    def shouldFlush(self, record):
        """no longer flushing if we reach flushLevel
        ... otherwise we get bombarded with emails
        """
        return len(self.buffer) >= self.capacity

    def emit(self, record):
        try:
            self.buffer.append(record)
            if self.shouldFlush(record):
                self.flush()
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

    def flush(self):
        if not self.buffer:
            return
        try:
            msg = self._build_html_msg(self.buffer[-1])  # last msg success/fail
            formatted = [self._format_record(_) for _ in self.buffer]
            text, html = zip(*formatted)
            text = "\n".join(text)
            html = '<html><head></head><body>{}</body></html>'\
                            .format("\n".join(html))
            msg.attach(MIMEText(text, 'text'))
            msg.attach(MIMEText(html, 'html'))
            self._send_html_msg(msg.as_string())
            self.buffer = []
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            for record in self.buffer:
                self.handleError(record)
            self.buffer = []

    def close(self):
        "final flush before closing the handler"
        self.flush()
        super(BufferedColoredSMTPHandler, self).close()


class ColoredMandrillHandler(ColoredHandler, logging.Handler):
    """send logging emails via mandrill http api instead of smtp
    """

    def __init__(self, apikey, fromaddr, toaddrs, subject):
        logging.Handler.__init__(self)
        self.api = mandrill.Mandrill(apikey)
        self.fromaddr = fromaddr
        if isinstance(toaddrs, basestring):
            toaddrs = [toaddrs]
        self.toaddrs = [{'email': email} for email in toaddrs]
        self.subject = subject

    def emit(self, record):
        text, html = self._format_record(record)
        msg = {
            'from_email': self.fromaddr,
            'to': self.toaddrs,
            'subject': self.getSubject(record),
            'html': html,
            'text': text,
        }
        try:
            self.api.messages.send(message=msg, async=True)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)


class ScreenshotColoredMandrillHandler(ColoredMandrillHandler):
    """Mandrill version of our ScreenshotColoredSMTPHandler
    """
    def __init__(self, apikey, fromaddr, toaddrs, subject, **kw):
        self.webdriver = kw.pop('webdriver', None)
        super(ScreenshotColoredMandrillHandler, self)\
                .__init__(apikey, fromaddr, toaddrs, subject, **kw)

    def emit(self, record):
        if self.webdriver is None:  # lose the message
            return
        name = 'screenshot.png'
        src_name = 'page_source.txt'
        try:
            text, html = self._format_record(record)
            url = self.webdriver.current_url
            lk = '<div><a href="{}">{}</a></div>'.format(url, url)
            html = '<html><head></head><body>{}{}<img src="cid:{}"/></body></html>'\
                            .format(html, lk, name)
            img = {
                'content': self.webdriver.get_screenshot_as_base64(),
                'name': name,
                'type': 'image/png',
                }
            src = {
                'content': self.webdriver.page_source.encode('utf-8'),
                'name': src_name,
                'type': 'text/plain',
                }
            msg = {
                'from_email': self.fromaddr,
                'to': self.toaddrs,
                'subject': self.getSubject(record),
                'html': html,
                'text': text,
                'images': [img],
                'attachments': [src],
            }
            self.api.messages.send(message=msg, async=True)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)


class URLHandler(HTTPHandler):
    """HTTPHandler with HTTPS a SumoLogic headers
    """
    def __init__(self, host, url, method):
        super(URLHandler, self).__init__()
        self.host = host
        self.url = url
        self.method = method

    def emit(self, record):
        try:
            data = self.format(record)
            with closing(urllib2.urlopen(self.host+self.url, data)) as req:
                _ = req.read()
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)


## TODO class logging, stolen from the bowels of sqlalchemy
#
#def _add_default_handler(logger):
#    handler = logging.StreamHandler(sys.stdout)
#    handler.setFormatter(logging.Formatter(
#        '%(asctime)s %(levelname)s %(name)s %(message)s'))
#    logger.addHandler(handler)
#
#
#_logged_classes = set()
#
#
#def class_logger(cls, enable=False):
#    logger = logging.getLogger(cls.__module__ + "." + cls.__name__)
#    if enable == 'debug':
#        logger.setLevel(logging.DEBUG)
#    elif enable == 'info':
#        logger.setLevel(logging.INFO)
#    cls._should_log_debug = lambda self: logger.isEnabledFor(logging.DEBUG)
#    cls._should_log_info = lambda self: logger.isEnabledFor(logging.INFO)
#    cls.logger = logger
#    _logged_classes.add(cls)


def set_level(levelname):
    "simple utility for setting root logging via sqla"
    level = logging._levelNames[levelname.upper()]
    for handler in logging.root.handlers:
        handler.setLevel(level)
    logging.root.setLevel(level)


def stream_is_tty(somestream):
    "check if stream, typically sys.stdout, running in terminal"
    isatty = getattr(somestream, 'isatty', None)
    return isatty and isatty()


def log_exception(logger):
    "return wrapped function fn in try except and log exception with logger"
    def wrapper(fn):
        @wraps(fn)
        def wrapped_fn(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except Exception as exc:
                logger.exception(exc)
                raise exc
        return wrapped_fn
    return wrapper


if __name__ == '__main__':
    logger = logging.getLogger('job')
    logger.setLevel(logging.DEBUG)
    logger.addHandler(ColoredStreamHandler())
    logger.debug('Debug')
    logger.info('Info')
    logger.warning('Warning')
    logger.error('Error')
    try:
        1 / 0
    except Exception as exc:
        logger.exception(exc)
