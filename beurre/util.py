# encoding: utf-8

from __future__ import with_statement

import csv
import random
import re
import string
import sys
import math
import difflib
import os
import operator
import copy
import base64
import unicodedata
import datetime
import signal
from collections import defaultdict, Mapping, namedtuple
from itertools import (
        islice,
        permutations,
        chain,
        tee,
        ifilterfalse,
        cycle,
        izip_longest,
        )
from functools import wraps, reduce, update_wrapper
from cStringIO import StringIO
from contextlib import contextmanager
from zipfile import ZipFile, ZipInfo
from operator import mul

import chardet

import logging
logger = logging.getLogger(__name__)


#
# useful constants for writing unicode-based context-free grammars
#

UNI_ALL = u''.join(unichr(_) for _ in xrange(65536))
UNI_DECIMALS = u''.join(_ for _ in UNI_ALL if unicodedata.category(_)=='Nd')
UNI_SLASHES = unichr(47)+unichr(8260)+unichr(8725)
UNI_SUPERSCRIPTS = unichr(8304)+unichr(185)+unichr(178)+unichr(179)+u''.join(unichr(_) for _ in xrange(8308, 8314))
UNI_SUBSCRIPTS = u''.join(unichr(_) for _ in xrange(8320, 8330))
UNI_VULGAR_FRACTIONS = unichr(188)+unichr(189)+unichr(190)+u''.join(unichr(_) for _ in xrange(8531, 8543))

SUPERSCRIPT = dict(zip(UNI_SUPERSCRIPTS, range(10)))
SUBSCRIPT = dict(zip(UNI_SUBSCRIPTS, range(10)))

_VULGAR_FRACTIONS = (
    1/4.,
    2/4.,
    3/4.,
    1/3.,
    2/3.,
    1/5.,
    2/5.,
    3/5.,
    4/5.,
    1/6.,
    5/6.,
    1/8.,
    3/8.,
    5/8.,
    7/8.,
    )
VULGAR_FRACTION = dict(zip(UNI_VULGAR_FRACTIONS, _VULGAR_FRACTIONS))


#
# class and function manipulations
#

def attrs(*attrnames):
    """lazily stuff in get/setters

    >>> class Foo:
    ...     _a = 1
    ...     _b = 2
    ...     _c = 3
    ...     attrs('a', 'b', 'c')
    ...     _z = (_a, _b, _c,)
    ...     z = property(lambda x: x._z)

    vanilla attrs work fine
    >>> f = Foo()
    >>> f.a
    1
    >>> f.a+f.b==f.c
    True

    beware of link between cls._a and self._a
    >>> f.a = 2
    >>> f.a
    2
    >>> f._a
    1

    we can also do lazy definitions like our `z`
    >>> len(f.z)==3
    True
    >>> sum(f.z)==6
    True
    >>> f.z[0]==f._z[0]==1
    True
    >>> f.z = (4, 5, 6,)
    >>> sum(f.z)
    15
    >>> f.a==2
    True
    """
    def makeprop(name):
        return property(lambda self: getattr(self, '_%s'% name))
    caller_locals = sys._getframe(1).f_locals
    for attrname in attrnames:
        caller_locals[attrname] = makeprop(attrname)


@contextmanager
def replacekey(d, key, newval):
    """handy bugger for temporarily patching a dict

    >>> f = dict(x=13)
    >>> with replacekey(f, 'x', 'pho'):
    ...     f['x']
    'pho'
    >>> f['x']
    13

    if the dict does not have the key set before, we return to that state
    >>> rand_key = os.urandom(10)
    >>> with replacekey(os.environ, rand_key, '22'):
    ...     os.environ[rand_key]=='22'
    True
    >>> rand_key in os.environ
    False
    """
    wasset = key in d
    oldval = d.get(key)
    d[key] = newval
    yield
    if wasset:
        d[key] = oldval
    else:
        del d[key]


@contextmanager
def replaceattr(obj, attrname, newval):
    """handy bugger for temporarily monkey patching an object

    >>> class Foo: pass
    >>> f = Foo()
    >>> f.x = 13
    >>> with replaceattr(f, 'x', 'pho'):
    ...     f.x
    'pho'
    >>> f.x
    13

    if the obj did not have the attr set, we remove it
    >>> with replaceattr(f, 'y', 'boo'):
    ...     f.y=='boo'
    True
    >>> hasattr(f, 'y')
    False
    """
    wasset = hasattr(obj, attrname)
    oldval = getattr(obj, attrname, None)
    setattr(obj, attrname, newval)
    yield
    if wasset:
        setattr(obj, attrname, oldval)
    else:
        delattr(obj, attrname)


class timeout:
    """with statement to manage timeouts for potential hanging code
    http://stackoverflow.com/a/22348885/424380

    >>> import time
    >>> with timeout(1):
    ...     time.sleep(2)
    ...     print "foo"
    Traceback (most recent call last):
        ...
    OSError: Timeout!!
    >>> with timeout(1):
    ...     print "foo"
    foo
    """
    def __init__(self, seconds=100, error_message='Timeout!!'):
        self.seconds = seconds
        self.error_message = error_message

    def handle_timeout(self, signum, frame):
        raise OSError(self.error_message)

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handle_timeout)
        signal.alarm(self.seconds)

    def __exit__(self, type, value, traceback):
        signal.alarm(0)


class attrdict(dict):
    """the vanilla attrdict everyone has in their utils

    >>> d = attrdict(x=10, y='foo')
    >>> d.x
    10
    >>> d['x']
    10
    >>> d.y = 'baa'
    >>> d['y']
    'baa'
    >>> g = d.copy()
    >>> g.x = 11
    >>> d.x
    10
    >>> tricky = [d, g]
    >>> tricky2 = copy.copy(tricky)
    >>> tricky2[1].x
    11
    >>> tricky2[1].x = 12
    >>> tricky[1].x
    12
    >>> righty = copy.deepcopy(tricky)
    >>> righty[1].x
    12
    >>> righty[1].x = 13
    >>> tricky[1].x
    12
    """

    def __getattr__(self, attrname):
        if not attrname in self:
            raise AttributeError(attrname)
        return self[attrname]

    def __setattr__(self, attrname, attrval):
        self[attrname] = attrval

    def __delattr__(self, attrname):
        if not attrname in self:
            raise AttributeError(attrname)
        self.pop(attrname)

    def update(self, *args, **kwargs):
        dict.update(self, *args, **kwargs)
        return self

    def copy(self, **kwargs):
        newdict = attrdict(dict.copy(self))
        return newdict.update(**kwargs)


class lazydict(attrdict):
    """attrdict where functions get stored as lazy calculations

    >>> a = lazydict(a=1, b=2, c=lambda x: x.a+x.b)
    >>> a.c
    3
    >>> a.a = 99
    >>> a.c
    101

    make sure we dont share descriptors, could cause awful bugs
    >>> z = lazydict(a=2, y=4, f=lambda x: x.a*x.y)
    >>> z.b
    Traceback (most recent call last):
        ...
    AttributeError: b
    >>> z.c
    Traceback (most recent call last):
        ...
    AttributeError: c
    >>> z.f
    8

    a's attrs should be unaffected
    >>> a.f
    Traceback (most recent call last):
        ...
    AttributeError: f
    """
    def __getattr__(self, attrname):
        if attrname not in self:
            raise AttributeError(attrname)
        attrval = self[attrname]
        if callable(attrval):
            return attrval(self)
        return attrval


def include(source, names=()):
    """include greedy classproperty dict in declaration

    >>> d = dict(x=10, y='foo')
    >>> class Foo:
    ...     include(d)

    >>> Foo.x
    10
    >>> Foo.y
    'foo'
    >>> class Boo:
    ...     include(d, ('y',))

    >>> hasattr(Boo, 'x')
    False
    >>> hasattr(Boo, 'y')
    True
    """
    sys._getframe(1).f_locals.update(
        dict((name,source[name]) for name in names) if names else source)


def singleton(cls):
    """elegant singleton enforcement via python decorators wiki

    >>> @singleton
    ... class Highlander:
    ...     _x = 100
    ...     _y = 'y'
    ...     attrs('x', 'y')

    >>> H = Highlander
    >>> H() is H() is H
    True
    >>> id(H()) == id(H)
    True
    >>> h = H()
    >>> h.x == H().x == H.x == 100
    True
    >>> H.x = 50
    >>> h.x == H().x == H.x == 50
    True

    >>> hc = copy.deepcopy(h)
    >>> HC = copy.deepcopy(H)
    >>> hc.y==h.y==H.y==HC.y=='y'
    True
    """
    instance = cls()
    instance.__call__ = lambda: instance
    return instance


def memoize(obj):
    """keep dict of function calls as a function attribute
    re: http://stackoverflow.com/a/3243694/424380
    re: https://wiki.python.org/moin/PythonDecoratorLibrary#Memoize
    NOTE: we also have one from web.py which is nice ...

    unique n-length arrays of ints whose abs val sums to k:
    V(n,0)=1; V(0,k)=0; V(n,k) = V(n-1,k) + V(n,k-1) + V(n-1,k-1);

    >>> def n_with_sum_k(n, k):
    ...     if n==0:
    ...         return 0
    ...     elif k==0:
    ...         return 1
    ...     else:
    ...         less_n = n_with_sum_k(n-1, k)
    ...         less_k = n_with_sum_k(n, k-1)
    ...         less_both = n_with_sum_k(n-1, k-1)
    ...         return less_n + less_k + less_both

    >>> n_with_sum_k_mz = memoize(n_with_sum_k)
    >>> n_with_sum_k_mz(3, 5)
    61
    >>> n_with_sum_k_mz.cache
    {'(3, 5){}': 61}
    """

    cache = obj.cache = {}

    @wraps(obj)
    def memoizer(*args, **kwargs):
        key = str(args) + str(kwargs)
        if key not in cache:
            cache[key] = obj(*args, **kwargs)
        return cache[key]
    return memoizer


class classproperty(property):
    """decorator like @property for classes instead of instances

    >>> class Foo:
    ...     include(dict(a=1, b=2))
    ...     @classproperty
    ...     def c(cls):
    ...         return cls.a+cls.b

    >>> Foo.a
    1
    >>> Foo.b
    2
    >>> Foo.c
    3
    >>> Foo.a = 2
    >>> Foo.c
    4
    """
    def __get__(desc, self, cls):
        return desc.fget(cls)


def delegate(deleg, attrs):
    """delegate methods to other objects attached to your object

    >>> class X:
    ...     a = 1

    >>> class Y:
    ...     x = X()
    ...     delegate('x', 'a')

    >>> Y().a
    1

    >>> class A:
    ...     def echo(self, x):
    ...         print x

    >>> class B:
    ...     a = A()
    ...     delegate('a', ['echo'])

    >>> B().echo('whoa!')
    whoa!
    """
    def _makeprop(attr):
        return property(lambda self: getattr(getattr(self, deleg), attr))

    caller_locals = sys._getframe(1).f_locals
    for attr in attrs:
        caller_locals[attr] = _makeprop(attr)


def lazy_property(fn):
    """Decorator that makes a property lazy-evaluated.

    >>> import time
    >>> class Sloth(object):
    ...     def _slow_cool(self, n):
    ...         time.sleep(n)
    ...         return n**2
    ...     @lazy_property
    ...     def slow(self):
    ...         return True
    ...     @lazy_property
    ...     def cool(self):
    ...         return self._slow_cool(3)
    >>> x = time.time()
    >>> s = Sloth()
    >>> time.time()-x < 1
    True
    >>> time.time()-x < 1
    True
    >>> hasattr(s, '_lazy_slow')
    False
    >>> s.slow
    True
    >>> hasattr(s, '_lazy_slow')
    True
    >>> s.cool
    9
    >>> 3 < time.time()-x < 6
    True
    >>> s.cool
    9
    >>> 3 < time.time()-x < 6
    True
    """
    attr_name = '_lazy_' + fn.__name__

    @property
    def _lazy_property(self):
        if not hasattr(self, attr_name):
            setattr(self, attr_name, fn(self))
        return getattr(self, attr_name)
    return _lazy_property


def compose(*functions):
    """return a function folding over a list of functions
    each arg must have a single param, so that it is explicit

    >>> f = lambda x: x+4
    >>> g = lambda y: y/2
    >>> h = lambda z: z*3
    >>> fgh = compose(f, g, h)

    beware of order for non-commutative functions (first in, **last** out)
    >>> fgh(2)==h(g(f(2)))
    False
    >>> fgh(2)==f(g(h(2)))
    True
    """
    return reduce(lambda f,g: lambda x: f(g(x)), functions)


def composable(decorators):
    """decorator that takes a list of decorators to be composed
    useful when list of decorators starts getting large and unruly

    >>> def m3(func):
    ...     def wrapped(n):
    ...         return func(n)*3.
    ...     return wrapped

    >>> def d2(func):
    ...     def wrapped(n):
    ...         return func(n)/2.
    ...     return wrapped

    >>> def p3(n):
    ...     return n+3.

    >>> @m3
    ... @d2
    ... def plusthree(x):
    ...     return p3(x)

    >>> @composable([d2, m3])
    ... def cplusthree(x):
    ...     return p3(x)

    Despite the similar name, composed decorators are not
    interchangeable with `compose` for standard functions,
    since decorators return functions, not the func output
    >>> func = compose(m3, d2, p3)(4)
    >>> hasattr(func, '__call__')
    True
    >>> compose(lambda n: n*3., lambda n: n/2., p3)(4)
    10.5

    what they do allow is consolidating longer decorator chains
    >>> plusthree(4)
    10.5
    >>> cplusthree(4)
    10.5
    """
    def composed(func):
        if hasattr(decorators, '__iter__'):
            for dec in decorators[::-1]:
                func = dec(func)
            return func
        return decorators(func)

    def wrapped(func):
        @wraps(func)
        def f(*a, **kw):
            return composed(func)(*a, **kw)
        return f

    return wrapped


#
# list like methods from different work
#

def compact(iterable):
    """get the none junk out of an iterable -- also removes zero!!

    >>> compact([0,2,3,4,None,5])
    (2, 3, 4, 5)
    """
    return tuple(item for item in iterable if item)


def groupby(iterable, keyfunc):
    groups = defaultdict(list)
    for item in iterable:
        groups[keyfunc(item)].append(item)
    return groups


def accumulate(iterable, func=operator.add):
    """now a part of python 3, but used to just be missing

    >>> [_ for _ in accumulate([1,2,3,4,5])]
    [1, 3, 6, 10, 15]
    >>> ''.join(accumulate(['a','b','c']))
    'aababc'
    """
    it = iter(iterable)
    total = next(it)
    yield total
    for element in it:
        total = func(total, element)
        yield total


def unique(iterable):
    """for optimizing away your set()'s

    >>> a = (1,1,2,3,5,9,3,4,5,6)
    >>> set_a = set(a)
    >>> uq_a = unique(a)
    >>> len(set_a)==len(uq_a)
    True
    """
    already_seen = set()
    result = []
    for item in iterable:
        if item not in already_seen:
            already_seen.add(item)
            result.append(item)
    return result


def hashby(iterable, keyfunc):
    return dict((keyfunc(item), item,) for item in iterable)


def invert(dct):
    return dict((v, k,) for k,v in dct.items())


def mapkeys(func, dct):
    return dict((func(key), val,) for key, val in dct.items())


def mapvals(func, dct):
    return dict((key, func(val),)for key, val in dct.items())


def divide(iterable, size):
    """split an iterable into sub-iterables of length `size`

    >>> list(divide(range(10), 5))
    [(0, 1, 2, 3, 4), (5, 6, 7, 8, 9)]
    """
    iterable = iter(iterable)
    while True:
        chunk = tuple(islice(iterable, size))
        if chunk:
            yield chunk
        if len(chunk) < size:
            break


def getitem(sequence, index, default=None):
    if index < len(sequence):
        return sequence[index]
    else:
        return default


def negate_permute(*items):
    """for each item in iterable items,
    rebuild tuple with just the one item negated,
    get all permutations of tuple with negated item
    ~= permutation of each + and - version of each item

    >>> negate_permute(1, 2).next()
    (-1, 1, -2, 2)
    >>> negate_permute(-float('inf'), 0).next()
    (inf, -inf, 0, 0)
    """
    for permuted in permutations(chain(*((-a,a) for a in items))):
        yield permuted


def choose(n,k):
    """simple implementation of n choose k

    >>> choose(10, 3)
    120
    """
    return int(round(reduce(mul, (float(n-i)/(i+1) for i in range(k)), 1)))


def random_string(length):
    return ''.join(random.choice(string.ascii_uppercase+string.digits)
                    for x in range(length))


def base64file(fil):
    return base64.encodestring(open(fil, "rb").read())


#
# file i/o classes and functions
#

def render_csv(rows, dialect=csv.excel):
    """quick helper routine to output a csv string.
    """
    f = StringIO()
    writer = csv.writer(f, dialect=dialect)
    for row in rows:
        writer.writerow(row)
    return f.getvalue()


class CsvZip(ZipFile):
    """zipped csv file that handles file permissions correctly on dos
    re: http://stackoverflow.com/q/279945/424380
    """

    @property
    def value(self):
        self.close()
        return self.__buffer.getvalue()

    def __init__(self):
        self.__buffer = StringIO()
        ZipFile.__init__(self, self.__buffer, 'w')

    def writecsv(self, filename, data):
        info = ZipInfo('%s.csv'% filename)
        info.external_attr = 0644 << 16L
        self.writestr(info, render_csv(data))


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno==os.errno.EEXIST:
            pass
        else:
            raise


#
# mathematics utils in pure python, e.g., for in-mem calcs in webservers
#

def is_numeric(txt):
    """call something a number if we can force it into a float
    WARNING: complex types cannot be converted to float

    >>> is_numeric('a')
    False
    >>> is_numeric(1e4)
    True
    >>> is_numeric('1E2')
    True
    >>> is_numeric(complex(-1,0))
    False
    """
    try:
        float(txt)
        return True
    except (ValueError, TypeError):
        return False


registry = {}

class MultiMethod:
    """multimethod that supports args no kwargs (by design ...)
    via bdfl http://www.artima.com/weblogs/viewpost.jsp?thread=101605

    @multimethod(int, int)
    """
    def __init__(self, name):
        self.name = name
        self.typemap = {}

    def __call__(self, *args):
        types = tuple(arg.__class__ for arg in args)
        function = self.typemap.get(types)
        if function is None:
            raise TypeError("no match")
        return function(*args)

    def register(self, types, function):
        if types in self.typemap:
            raise TypeError("duplicate registration")
        self.typemap[types] = function


def multimethod(*types):
    def register(function):
        name = function.__name__
        mm = registry.get(name)
        if mm is None:
            mm = registry[name] = MultiMethod(name)
        mm.register(types, function)
        return mm
    return register


def peel(str_or_iter):
    """peel iterator one by one, yield item, aliasor item, item

    >>> list(peel(["a", ("", "b"), "c"]))
    [('a', 'a'), ('', 'b'), ('c', 'c')]
    """
    things = (_ for _ in str_or_iter)
    while things:
        this = things.next()
        if isinstance(this, (tuple, list)):
            yield this
        else:
            yield this, this


def rpeel(str_or_iter):
    """peel iterator one by one, yield alias if tuple, else item"

    >>> list(rpeel(["a", ("", "b"), "c"]))
    ['a', 'b', 'c']
    """
    things = (_ for _ in str_or_iter)
    while things:
        this = things.next()
        if isinstance(this, (tuple, list)):
            yield this[-1]
        else:
            yield this


def backfill(prices):
    """back-fill a sorted array with the latest value

    >>> backfill([None, None, 1, 2, 3, None, 4])
    [1, 1, 1, 2, 3, 3, 4]
    >>> backfill([1,2,3])
    [1, 2, 3]
    >>> backfill([])
    []
    >>> backfill([1, 2, 3, None])
    [1, 2, 3, 3]
    """
    latest = None
    missing = 0  # at start
    filled = []
    for i, price in enumerate(prices):
        if price is not None:
            latest = price
            if missing:
                filled = [latest]*missing
                missing = 0
            filled.append(price)
        else:
            if latest is None:
                missing += 1
            else:
                filled.append(latest)
    return filled


def backfill_iterdict(iterdict):
    """back-fill a sorted iterdict with the latest value

    >>> backfill_iterdict([
    ...     {'a': 1, 'b': None},
    ...     {'a': 4, 'b': 2},
    ...     {'a': None, 'b': None},
    ...     {'a': 3, 'b': None}])
    [{'a': 1, 'b': 2}, {'a': 4, 'b': 2}, {'a': 4, 'b': 2}, {'a': 3, 'b': 2}]
    >>> backfill_iterdict([])
    []
    >>> backfill_iterdict([
    ...     {'a': 9, 'b': 2},
    ...     {'a': 4, 'b': 1},
    ...     {'a': 3, 'b': 4},
    ...     {'a': 3, 'b': 3}])
    [{'a': 9, 'b': 2}, {'a': 4, 'b': 1}, {'a': 3, 'b': 4}, {'a': 3, 'b': 3}]
    """
    latest = dict()
    missing = dict()  # front-fill w first value
    filled = []
    for i, _dict in enumerate(iterdict):
        this = dict()
        for k, v in _dict.items():
            if v is not None:
                latest[k] = v
                if missing.has_key(k):
                    for j in range(missing[k]):
                        filled[j][k] = latest[k]
                this[k] = v
            else:
                if latest.get(k) is None:
                    missing[k] = (missing.get(k) or 0) + 1
                else:
                    this[k] = latest[k]
        filled.append(this)
    return filled


def align(iterdict_a, iterdict_b, **kw):
    """given two lists of dicts ('iterdicts'), sorted on some attribute,
    build a single list with dicts, with keys within a given tolerance
    anything that cannot be aligned is DROPPED

    >>> zip(*align(
    ...     [{'a': 1}, {'a': 2}, {'a': 5}],
    ...     [{'b': 5}], a='a', b='b', diff=lambda x,y: x-y))
    [({'a': 5},), ({'b': 5},)]
    >>> zip(*align([{'b': 5}], [{'a': 1}, {'a': 2}, {'a': 5}],
    ...     a='b', b='a', diff=lambda x,y: x-y))
    [({'b': 5},), ({'a': 5},)]
    """
    attr_a = kw.get('a', 'date')
    attr_b = kw.get('b', 'date')
    tolerance = kw.get('tolerance', 0)
    diff = kw.get('diff', lambda x,y: (x-y).days)

    gen_a, gen_b = (_ for _ in iterdict_a), (_ for _ in iterdict_b)
    this_a, this_b = None, None
    while gen_a or gen_b:
        if not this_a or diff(this_a.get(attr_a), this_b.get(attr_b)) < tolerance:
            this_a = gen_a.next()
            logger.debug("Advanced A to {}".format(this_a.get(attr_a)))
        if not this_b or diff(this_a.get(attr_a), this_b.get(attr_b)) > tolerance:
            this_b = gen_b.next()
            logger.debug("Advanced B to {}".format(this_b.get(attr_b)))
        if abs(diff(this_a.get(attr_a), this_b.get(attr_b))) <= tolerance:
            logger.debug("Aligned iters to A {} B {}"\
                    .format(this_a.get(attr_a), this_b.get(attr_b)))
            yield this_a, this_b
            this_a, this_b = gen_a.next(), gen_b.next()


def partition(pred, iterable):
    """partition an iterable by False/True of function `pred`
    - via python docs itertools examples

    >>> falses, trues = partition(lambda x: x==1, [1, 2, 3, 1, 2, 3])
    >>> list(falses)
    [2, 3, 2, 3]
    >>> trues
    [1, 1]
    """
    t1, t2 = tee(iterable)
    return ifilterfalse(pred, t1), filter(pred, t2)


def roundrobin(*iterables):
    """pluck one at a time from an arbitrary number of iterables
    - Recipe credited to George Sakkis

    >>> " ".join(roundrobin('ABC', 'D', 'EF'))
    'A D E B F C'
    """
    pending = len(iterables)
    nexts = cycle(iter(it).next for it in iterables)
    while pending:
        try:
            for next in nexts:
                yield next()
        except StopIteration:
            pending -= 1
            nexts = cycle(islice(nexts, pending))


def grouper(n, iterable, fillvalue=None):
    """group list into list of sublists

    >>> [''.join(_) for _ in grouper(3, 'ABCDEFG', 'x')]
    ['ABC', 'DEF', 'Gxx']
    """
    args = [iter(iterable)] * n
    return izip_longest(fillvalue=fillvalue, *args)


def scriptname(task=None):
    """return name of script being run, without the file extension

    >>> scriptname(__file__)
    'util'
    >>> scriptname() in sys.argv[0]
    True
    >>> scriptname()==sys.argv[0]
    False
    """
    task = task or sys.argv[0]
    if task:
        app, _ = os.path.splitext(os.path.basename(task))
    else:
        app = ""
    return app


def ismapping(something):
    """does this look kinda sorta like a dict?

    >>> ismapping(dict())
    True
    >>> ismapping(attrdict())
    True
    """
    return isinstance(something, Mapping)


def merge_dict(old, new):
    """modify old mapping in place to contain new mapping

    multilevel merging
    >>> xx = {'a': {'b': 1, 'c': 2}, 'b': 2}
    >>> nice = {'a': {'a': 9}, 'c': 3}

    >>> merge_dict(xx, nice)
    >>> 'a' in xx['a']
    True
    >>> 'c' in xx
    True

    warning, it will overwrite stuff
    >>> warn = {'a': {'c': 9}, 'b': 3}
    >>> merge_dict(xx, warn)
    >>> xx['a']['c']
    9
    >>> xx['b']
    3

    tries to be smart with iterables, but does *not* force types
    >>> l1 = {'a': {'c': [5, 2]}, 'b': 1}
    >>> l2 = {'a': {'c': [1, 2]}, 'b': 3}
    >>> l3 = {'a': {'c': (1, 2,)}, 'b': 3}
    >>> merge_dict(l1, l2)
    >>> len(l1['a']['c'])
    4
    >>> l1['b']
    3
    >>> merge_dict(l1, l3)
    Traceback (most recent call last):
        ...
    TypeError: can only concatenate list (not "tuple") to list
    """
    for key, new_val in new.iteritems():
        old_val = old.get(key)
        if ismapping(old_val) and ismapping(new_val):
            merge_dict(old_val, new_val)
        elif hasattr(old_val, '__iter__') and hasattr(new_val, '__iter__'):
            old[key] = old_val+new_val
        else:
            old[key] = new_val


def fuzzy_search(search_term, items):
    """Search for term in a list of items with one or more terms
    Scores each lower-cased "word" (split by space, -, and _) separately
    Returns the highest score **very** brute force, FIXME improve it

    >>> results = fuzzy_search("OCR",
    ...     [("Omnicare", "OCR",), ("Ocra", "OKK"), ("GGG",)])
    >>> (_,ocr_score), (_,okk_score), (_,ggg_score) = results
    >>> ocr_score
    1.0
    >>> okk_score  # doctest: +ELLIPSIS
    0.85...
    >>> ggg_score
    0.0
    >>> zip(*fuzzy_search("Ramco-Gers",
    ...     [("RAMCO-GERSHENSON PROPERTIES", "RPT US Equity",),
    ...      ("Ramco Inc.", "RMM123FAKE")]))[1]
    (1.0, 1.0)
    """
    score_words = lambda a,b: difflib.SequenceMatcher(a=a, b=b).ratio() if a and b else 0.
    lower_split = lambda x: re.split("[\s\-_]", x.lower())
    for item in items:
        _max = max(score_words(word, search_word)
                for term in item
                    for word in lower_split(term)
                        for search_word in lower_split(search_term))
        yield item, _max


def truncate(s, width, suffix='...'):
    """Truncate a string to max width chars
    Add the suffix if the string was truncated

    >>> truncate('fubarbaz', 6)
    'fub...'
    >>> truncate('fubarbaz', 3)
    Traceback (most recent call last):
        ...
    AssertionError: Desired width must be longer than suffix
    >>> truncate('fubarbaz', 3, suffix='..')
    'f..'
    """
    assert width > len(suffix), "Desired width must be longer than suffix"
    if len(s) <= width:
        return s
    w = width - len(suffix)
    # if the boundary is on a space, don't include it
    if s[w].isspace():
        return s[:w] + suffix
    # break on the first whitespace from the end
    return s[:w].rsplit(None, 1)[0] + suffix


def rotate(s):
    """Apply rot13-like translation to string, including digits and punctuation

    >>> rotate("foobarbaz")
    ';^^-,{-,E'
    """
    instr = string.lowercase + \
            string.digits + \
            string.punctuation + \
            string.uppercase
    midpoint = len(instr)/2
    outstr = instr[midpoint:] + instr[:midpoint]
    return string.translate(s, string.maketrans(instr, outstr))


def unicode_or_bust(obj, encoding='utf-8', errors='strict'):
    """for sanity, always decode to unicode string ASAP
    via [_Unicode in Python_ by Kumar McMillan][http://farmdev.com/talks/unicode/]

    >>> vanilla = u'Ivan Krsti\u0107'
    >>> stringy = 'Ivan Krstic'
    >>> encoded = vanilla.encode('utf-8')
    >>> unicode_or_bust(vanilla)==vanilla
    True
    >>> isinstance(unicode_or_bust(stringy), unicode)
    True
    >>> unicode_or_bust(encoded)==vanilla
    True
    """
    if isinstance(obj, basestring) and not isinstance(obj, unicode):
        obj = unicode(obj, encoding, errors)
    return obj


def smart_base64(s):
    """Additional intelligent defaults for en/decoding base 64
    useful as python 2.x email.utils._bdecode is a naive base64.decodestring

    what we need to do in real life:

    #. split out encoded words per
       [RFC 2047, Section 2](http://tools.ietf.org/html/rfc2047#section-2)
    #. flush the string parts with = placeholders until we get valid base64

    >>> smart_base64('=?utf-8?B?U1RaOiBGNFExNSBwcmV2aWV3IOKAkyBUaGUgc3RhcnQgb2YgdGh'
    ...              'lIGNhc2ggcmV0dXJuIHN0b3J5PyBQYXRoIHRvICQyMDAgc3RvY2sgcHJpY2U/?=')
    u'STZ: F4Q15 preview \u2013 The start of the cash return story? Path to $200 stock price?'

    common bug in email subjects is that multiline subjects are base64 encoded
    *per line* so we get non-base64 characters:
    >>> smart_base64('=?UTF-8?B?JDEwTU0rIENJVCBHUk9VUCBUUkFERVMgLSBDSVQgNScyMiAxMDLi'
    ...              'hZ0tMTAz4oWbICBNSw==?=\\r\\n\\t=?UTF-8?B?VA==?=')
    u"$10MM+ CIT GROUP TRADES - CIT 5'22 102\u215d-103\u215b  MK T"

    this one specifies UTF-8 but it is actually encoding Latin-1 characters for
    the 3/4's, 1/8's, 3/4's, 1/8's, 1/2's:
    >>> smart_base64('=?UTF-8?B?TVMgZW5lcmd5OiByaWcgMTdzIDkxwr4vOTLihZsgMThzIDkzwr4v'
    ...              'OTTihZsgMjBzIDgywg==?=\\r\\n\\t=?UTF-8?B?vS84Mw==?=')
    u'MS energy: rig 17s 91\\xc2\\xbe/92\\xe2\\x85\\x9b 18s 93\\xc2\\xbe/94\\xe2\\x85\\x9b 20s 82\\xc2 \\xbd/83'
    """
    if not s:
        return s
    parts = [_ for _ in s.split(u'?') if _.strip(u'=')]
    flush = lambda x: x + u'=' * (4 - len(x) % 4)
    decode = lambda x: ('-' in x or "\\" in x) and base64.urlsafe_b64decode(x) \
                or base64.standard_b64decode(x)
    if len(parts)==1:
        return decode(flush(s))
    if len(parts)==3:
        encoding, base, target = parts
        if base.lower()=='b':
            logger.debug(u"Detected base64 indicator 'B' in message")
            target = decode(flush(target))
        else:
            logger.debug(u"Assuming already 'Q' (quoted printable) message")
        for enc in (encoding, 'utf8', 'latin1',):
            logger.debug(u"Attempting to decode message to {}".format(enc))
            try:
                target = unicode_or_bust(target, enc)
                break
            except (LookupError, TypeError, UnicodeDecodeError,) as exc:
                logger.exception(exc)
        else:
            logger.warn("Attempting to decode message using chardet suggestion")
            detected = chardet.detect(target)['encoding']
            try:
                target = unicode_or_bust(target, detected)
            except (LookupError, TypeError, UnicodeDecodeError,) as exc:
                logger.exception(exc)
                target = unicode_or_bust(target, encoding, 'replace')
        return target
    lines = [_.strip() for _ in s.splitlines()]
    if len(lines) > 1:
        logger.debug("Interpreting as multiple base64 messages with {} breaks"\
                        .format(len(lines)))
        return u' '.join(smart_base64(_) for _ in lines)
    logger.error("Cannot handle {} base 64 encoding words: {}"\
                    .format(len(parts), s))
    return s


#
# db-api utilities
#

def chunked(cursor, size=1000):
    while True:
        this_chunk = cursor.fetchmany(size)
        if not this_chunk:
            break
        for this in this_chunk:
            yield this


#
# mercator projections commonly used on geography projects
#

def merc_x(lon, r_major=6378137.0):
    """project longitude into mercator / radians from major axis

    >>> "{:0.3f}".format(merc_x(40.7484))
    '4536091.139'
    """
    return r_major*math.radians(lon)


def merc_y(lat, r_major=6378137.0, r_minor=6356752.3142):
    """project latitude into mercator / radians from major/minor axes

    >>> "{:0.3f}".format(merc_y(73.9857))
    '12468646.871'
    """
    if lat>89.5: lat=89.5
    if lat<-89.5: lat=-89.5
    eccent = math.sqrt(1-(r_minor/r_major)**2)
    phi = math.radians(lat)
    sinphi = math.sin(phi)
    con = eccent*sinphi
    com = eccent/2
    den = ((1.0-con)/(1.0+con))**com
    ts = math.tan((math.pi/2-phi)/2)/den
    y = 0.0-r_major*math.log(ts)
    return y


def format_phone(phone):
    """reformat phone numbers for display

    >>> format_phone('6877995559')
    '687-799-5559'
    """
    pstr = str(phone)
    parr = [pstr[-10:-7],pstr[-7:-4],pstr[-4:]]
    if len(pstr)>10:
        parr.insert(0,pstr[:-10])
    formatted_phone = "-".join(parr)
    return formatted_phone


Range = namedtuple('Range', ['start', 'end'])

def days_overlap(range_one, range_two, days=False):
    """test by how much two date ranges overlap
    if `days=True`, we return an actual day count,
    otherwise we just return if it overlaps True/False
    poached from Raymond Hettinger http://stackoverflow.com/a/9044111

    >>> FIRST = datetime.date(2016, 3, 1)
    >>> SECOND = datetime.date(2016, 3, 2)
    >>> PREV = datetime.date(2016, 3, 29)
    >>> LAST = datetime.date(2016, 3, 30)
    >>> days_overlap((FIRST, LAST,), (FIRST, LAST,))
    True
    >>> days_overlap((FIRST, LAST,), (SECOND, PREV,))
    True
    >>> days_overlap((FIRST, LAST,), (FIRST, LAST,), True)
    30
    >>> days_overlap((SECOND, PREV,), (FIRST, LAST,))
    True
    >>> days_overlap((SECOND, PREV,), (FIRST, LAST,), True)
    28
    >>> days_overlap((FIRST, PREV,), (SECOND, LAST,))
    True
    >>> days_overlap((SECOND, LAST,), (FIRST, PREV,))
    True
    >>> days_overlap((FIRST, SECOND,), (PREV, LAST,))
    False
    >>> days_overlap((PREV, LAST,), (FIRST, SECOND,))
    False
    >>> days_overlap((PREV, LAST,), (FIRST, SECOND,), True)
    -26
    """
    r1 = Range(*range_one)
    r2 = Range(*range_two)
    latest_start = max(r1.start, r2.start)
    earliest_end = min(r1.end, r2.end)
    overlap = (earliest_end - latest_start).days + 1
    if days:
        return overlap
    return overlap >= 0


def kryptophy(blah):
    "intentionally mysterious"
    return int('0x'+''.join(map(lambda x: hex(ord(x))[2:], blah)), 16)


def uncamel(camel):
    """uncamel something in camel case, for christ's sake!!
    http://stackoverflow.com/a/1176023

    >>> uncamel('CamelCase')
    'camel_case'
    >>> uncamel('CamelCamelCase')
    'camel_camel_case'
    >>> uncamel('Camel2Camel2Case')
    'camel2_camel2_case'
    >>> uncamel('getHTTPResponseCode')
    'get_http_response_code'
    >>> uncamel('get2HTTPResponseCode')
    'get2_http_response_code'
    >>> uncamel('HTTPResponseCode')
    'http_response_code'
    >>> uncamel('HTTPResponseCodeXYZ')
    'http_response_code_xyz'
    """
    uncased = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', camel)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', uncased).lower()


if __name__ == "__main__":
    from doctest import testmod
    testmod()
