def syncd(lock):
    """synchronize an arbitrary number of functions with shared lock
    yanked out of the python decorator wiki, adding an illustrative test

    >>> import time
    >>> from threading import Lock, Thread
    >>> from Queue import Queue
    >>> lock = Lock()
    >>> A = Queue()
    >>> A.put('b')

    >>> @syncd(lock)
    ... def alpha():
    ...     time.sleep(2)
    ...     A.put('a')

    >>> @syncd(lock)
    ... def omega():
    ...     return A.get()

    >>> a = Thread(target=alpha)
    >>> a.start()
    >>> A.empty()
    False
    >>> A.get()
    'b'
    >>> A.empty()
    True
    >>> omega()  # 2-second blocking comes here
    'a'
    """
    def wrap(f):
        def new_function(*args, **kw):
            lock.acquire()
            try:
                return f(*args, **kw)
            finally:
                lock.release()
        return new_function
    return wrap
