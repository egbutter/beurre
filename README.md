Beurre Utility Module
===


![beurre](https://bitbucket.org/egbutter/beurre/raw/master/beurre.jpg "Beurre - via wikipedia http://upload.wikimedia.org/wikipedia/commons/f/fd/Western-pack-butter.jpg")


Contents
---

#. `util.py`: general programming utility functions
#. `weblib.py`: web programming utility functions, esp. web.py and flask
#. `log.py`: extensions to logging module, for colorizing output, email alerts ..
#. `lru.py`: least recently used cache backport, yanked from activestate recipe
#. `sqla.py`: sqlalchemy utility extensions for windowing, profiling ..
#. `future.py`: future statement, yanked from activestate
#. `console.py`: unix and windows console colorizing support


---


** All of this code is public domain or LGPL .. please reuse freely **
